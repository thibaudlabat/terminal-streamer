 
import os, shutil, sys, json

if not os.path.isdir("build"):
    print("* Creating build folder")
    os.mkdir("build")

if not os.path.isdir("build/static"):
    os.mkdir("build/static")

if not os.path.isfile("build/config.json"):
    print("* Copying template config to build/config.json")
    shutil.copy("config_template.json","build/config.json")
    print("/!\ Please edit the config and re-run this tool.")
    sys.exit(0)

print("* Loading build/config.json")
with open("build/config.json","r") as f:
    config = json.load(f)

def build_copy(a:str,b:str):
    print("* Copying '{}' -> '{}'".format(a,b))
    shutil.copy(a,b)
build_copy("src/record.py","build/static/record.py")
build_copy("src/index.html","build/static/index.html")

def read_file(file:str):
    with open(file,"r") as f:
        return f.read()

def build_open(file:str):
    print("* Building : {}".format(file))
    return open(file,"w")



with build_open("build/server.py") as f:
    f.write("PORT={}\n".format(config["server_port"]))
    f.write("STATIC_URL=\"{}\"\n".format(config["static_url"]))
    f.write("SERVER_IP=\"{}\"\n".format(config["server_ip"]))
    f.write(read_file("src/server.py"))

with build_open("build/static/stream") as f:
    f.write("dest={}\n".format(config["dest"]))
    f.write("BASE_URL=\"{}\"\n".format(config["static_url"]))
    f.write(read_file("src/stream"))

with build_open("build/static/watch") as f:
    f.write("dest={}\n".format(config["dest"]))
    f.write("BASE_URL=\"{}\"\n".format(config["static_url"]))
    f.write(read_file("src/watch"))

with build_open("build/static/stream.sh") as f:
    f.write("dest={}\n".format(config["dest"]))
    f.write("server_ip=\"{}\"\n".format(config["server_ip"]))
    f.write("server_port=\"{}\"\n".format(config["server_port"]))
    f.write("BASE_URL=\"{}\"\n".format(config["static_url"]))
    f.write(read_file("src/stream.sh"))

with build_open("build/static/watch.sh") as f:
    f.write("dest={}\n".format(config["dest"]))
    f.write("server_ip=\"{}\"\n".format(config["server_ip"]))
    f.write("server_port=\"{}\"\n".format(config["server_port"]))
    f.write("BASE_URL=\"{}\"\n".format(config["static_url"]))
    f.write(read_file("src/watch.sh"))

with build_open("build/static/index.html") as f:
     f.write(read_file("src/index.html").replace("{URL}",config["static_url"]))

print("done.")