# terminal-streamer
Stream your terminal content over the internet without the hassle of configuring firewalls or installing anything (but the dependencies).

Dependencies (ubuntu) : python3-pip curl netcat

Based on Asciinema

![Animation](animation.gif)
Terminal streaming in Ubuntu 20.04 live VMs.
The two VMs have no direct network access to each other.

# TODO


asciinema streaming :
backend : au lieu des codes, [nom commun]+[adjectif] avec dico
choix du tag, avec par défaut un truc random

gestion clean des arguments, avec "r" ou "w", pas accepter "rarezr" et "wzenrr" (première lettre)

faire un serveur clean avec gestion correcte des erreurs, fermeture des clients en cas de fermeture du serveur, affichage dans la console des logs (connexion client/server de stream)
avec IPs, date/heure, bref un logging propre

servir en HTTP les scripts client/serveur, qui installent asciinema avec pip si nécessaire, dans /tmp

bash:
afficher erreurs clean si pas possible d'installer asciinema, ou si python pas présent (faire un python3 --version préliminaire)


gestion des logs ?

killing netcat properly on stream/watch


