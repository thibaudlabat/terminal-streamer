[ -d "$dest/asciinema" ] || pip install --target="$dest/" asciinema
cd "$dest"
{ echo "r $1"; } | nc "$server_ip" "$server_port" | python3 -m asciinema cat -
echo "Terminal stream has stopped."