curl -fs "$BASE_URL/record.py" > "$dest/record.py" || { echo "curl $BASE_URL/record.py failed"; exit; }
[ -d "$dest/asciinema" ] || pip install --target="$dest/" asciinema
mkdir -p "$dest/socket"
socket="$dest/socket/$RANDOM$RANDOM$RANDOM"
mkfifo "$socket"
{ echo "w "; cat "$socket"; } | nc "$server_ip" "$server_port" &
pid=$!
echo python3 "$dest/record.py" "$socket" "bash"; 
python3 "$dest/record.py" "$socket" "bash";
echo "Terminal stream has stopped."
kill "$pid" 2>/dev/null