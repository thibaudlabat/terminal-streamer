import sys
import asyncore
import socket
import threading
import time
import signal
import os
import random

LOG_LEVEL=3
# -1 : silent
# 0 : fatal error
# 1 : non-fatal errors
# 2 : warning
# 3 : info
# 4 : debug
def log(level:int,*args):
    if level<=LOG_LEVEL:
        print(*args)

class DevNullHandler(asyncore.dispatcher_with_send):
    pass

"""
socket state (custom):
0 : waiting for first request
1 : streaming to the server
2 : watching a stream

socket :
handler -> [state:int,streamid:str]

]

stream dict:
streamid -> [server_handler,{clients}, previous_data ]
"""

WRONG_CODE_MESSAGE="""{"version": 2, "width": 104, "height": 27, "timestamp": 1635524523, "env": {"SHELL": "/bin/bash", "TERM": "xterm-256color"}}
[0, "o", "$ "]
[0.210274, "o", "w"]
[0.495874, "o", "r"]
[0.912011, "o", "o"]
[1.262032, "o", "n"]
[1.752792, "o", "g"]
[2.181855, "o", " "]
[2.675952, "o", "c"]
[3.014832, "o", "o"]
[3.34895, "o", "d"]
[3.457841, "o", "e"]
[4.160507, "o", " "]
[4.512089, "o", ":"]
[4.612938, "o", ")"]
[5.315931, "o", "\\r\\n\\u001b[?2004l\\r"]"""

class Server(asyncore.dispatcher):
    "Receive connections and allocate a handler to each connection"

    handler = DevNullHandler

    def simulate_human(self,handler,message:str, t:float=5/20):
        first=True
        for L in message.split("\n"):
            s=""
            if not first: s+="\n"
            first=False
            s+=L
            handler.send( s.encode() )
            wait=abs(random.gauss(t,t/2))
            time.sleep(wait)
        

    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)
        self.handlers=dict()
        self.streams=dict()
    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            log(3,"Incoming connection from %s" % repr(addr))
            handler = self.handler(sock)
            self.handlers[handler]=([0,None])
            #handler.send(b"coucou\n")
            handler.handle_read=lambda:self.handle_read(handler)
            handler.handle_close=lambda:self.close_and_remove(handler)

    def handle_read(self,handler):
        read=handler.recv(100000)
        if not handler in self.handlers:
            log(4,"/!\\ reading from unregistered handler, ignored : ",handler)
            return
        state=self.handlers[handler][0]
        if state==0:
            if read[0]==ord('r'): # read
                log(4,"reader",handler)
                x = read.decode().split(" ")
                streamid = int(x[1])
                if not streamid in self.streams:
                    self.simulate_human(handler,WRONG_CODE_MESSAGE)
                    self.close_and_remove(handler)
                    return
                self.streams[streamid][1].add(handler)
                handler.send(self.streams[streamid][2])


            elif read[0]==ord("w"): # write
                log(4,"writer",handler)
                streamid = random.randrange(1000,9999)
                stream = [handler,set(),b""]
                self.streams[streamid]=stream
                s="\n"
                s+="You're now streaming your terminal.\n"
                s+="People can connect to your stream with :\n"
                s+="curl -s \"{}/watch\" | CODE={} bash".format(STATIC_URL,streamid)
                s+="\n"
                s+="\n"

                handler.send(s.encode())
                self.handlers[handler][0]=1
                self.handlers[handler][1]=streamid
            else:
                handler.send(b"command not found")
                self.close_and_remove(handler)
        elif state==1:
            streamid = self.handlers[handler][1]
            # broadcast message
            for client in self.streams[streamid][1]:
                client.send(read)
            # store for new clients
            self.streams[streamid][2] += read
            
        elif state==0:
            handler.send(b"readers are not supposed to write, exiting.")
            # detect&log writing ?
            self.close_and_remove(handler)
            
        else:
            handler.send(b"invalid state")
            self.close_and_remove(handler)

    def close_and_remove(self,handler):
        log(4,"exit : ",handler.socket)
        handler.close()
        if not handler in self.handlers:
            log(2, "(?) handler not registered ? ",handler)
            return
        handler_data = self.handlers.pop(handler)

        if handler_data[0]==2: # watching a stream
            streamid = handler_data[1]
            self.streams[streamid][1].remove(handler)
        elif handler_data[0]==1: # streaming
            streamid = handler_data[1]
            for client_handler in self.streams[streamid][1]:
                self.close_and_remove(client_handler)
            del self.streams[streamid]
        elif handler_data[0]==0: # initial state:
            pass
        else:
            log(2,"unimplemented invalid state exit")



if __name__ == '__main__':
    ip = "0.0.0.0"

    server = Server(ip, PORT)
    log(3,"Listening on %s:%d..." % (ip, PORT))

    class report_thread(threading.Thread):
        def __init__(self, event):
            threading.Thread.__init__(self)
            self.stopped = event
            self.report_interval = 5.0

        def run(self):
            while not self.stopped.isSet():
                self.stopped.wait(self.report_interval)
                self.report()
             

        def report(self):
            pass # executed every 5sec; todo: report
            

    stop_flag = threading.Event()
    t = report_thread(stop_flag)

    try:
        t.start()
        asyncore.loop()
    except:
        stop_flag.set()
